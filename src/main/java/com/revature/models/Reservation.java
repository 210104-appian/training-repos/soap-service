package com.revature.models;

import java.util.Date;

public class Reservation {

	private int id;
	private int partyCount;    
	private Date date;
	private String customerName;
	
	public Reservation() {
		super();
	}

	public Reservation(int id, int partyCount, Date date, String customerName) {
		super();
		this.id = id;
		this.partyCount = partyCount;
		this.date = date;
		this.customerName = customerName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPartyCount() {
		return partyCount;
	}

	public void setPartyCount(int partyCount) {
		this.partyCount = partyCount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customerName == null) ? 0 : customerName.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + id;
		result = prime * result + partyCount;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservation other = (Reservation) obj;
		if (customerName == null) {
			if (other.customerName != null)
				return false;
		} else if (!customerName.equals(other.customerName))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id != other.id)
			return false;
		if (partyCount != other.partyCount)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Reservation [id=" + id + ", partyCount=" + partyCount + ", date=" + date + ", customerName="
				+ customerName + "]";
	}
	
}

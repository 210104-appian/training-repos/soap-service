package com.revature.services;

import java.util.List;

import javax.jws.WebService;

import com.revature.models.Reservation;

@WebService
public interface ReservationService {
	// Service Endpoint Interface
	
	public List<Reservation> getAll();
	public Reservation getReservationById(int id);
	public List<Reservation> getReservationByCustomerName(String name);
	public boolean createNewReservation(Reservation r);
	
	
}

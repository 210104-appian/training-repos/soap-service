package com.revature.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.revature.models.Reservation;

public class ReservationServiceImpl implements ReservationService {

		private static List<Reservation> reservations = new ArrayList<>();
		
		static {
			reservations.add(new Reservation(101,3, new Date(2021, 10,9,10,30), "Tommy"));
			reservations.add(new Reservation(102,2,new Date(2021, 10,9,10,30), "Nina"));
			reservations.add(new Reservation(103,4,new Date(2021, 10,9,10,30), "Linda"));
			reservations.add(new Reservation(104,2,new Date(2021, 10,9,10,30), "Paul"));
			reservations.add(new Reservation(105,2,new Date(2021, 10,9,10,30), "Nina"));
		}
		
		public List<Reservation> getAll(){
			return new ArrayList<>(reservations);
		}
		
		public Reservation getReservationById(int id) {
			for(Reservation r: reservations) {
				if(r!=null && r.getId()==id) {
					return r;
				}
			}
			return null;
		}
		
		public List<Reservation> getReservationByCustomerName(String name){
			if(name==null) {
				return null;
			}
			List<Reservation> customerReservations = new ArrayList<>();
			for(Reservation r: reservations) {
				if(r!=null && name.equals(r.getCustomerName())) {
					customerReservations.add(r);
				}
			}
			return customerReservations;
		}
		
		public boolean createNewReservation(Reservation reservation) {
			return reservations.add(reservation);
		}
		
		


}
